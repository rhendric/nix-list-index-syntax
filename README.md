# Nix list index syntax patches

## What is this?

This is a NixOS module that adds a Nixpkgs overlay that adds patches to Nix that enable a syntax for indexing into lists.

You can choose from three patches that take three slightly different approaches.

With `"bracketed"`:

```
nix-repl> pkgs.maptool.meta.maintainers.[0].github
"rhendric"
```

With `"dot"`:

```
nix-repl> pkgs.maptool.meta.maintainers.0.github
"rhendric"
```

**Important:**
The `"dot"` patch also restricts float literal syntax.
With this patch (but not with the `"bracketed"` patch), float literals are now [required to start with a digit][why-dot].

With `"bang"`:

```
nix-repl> pkgs.maptool.meta.maintainers!0.github
"rhendric"
```

## How do I use it?

### As a NixOS module

Add the following to your NixOS configuration:

```nix
imports = [
  (builtins.fetchTarball {
    url = "https://gitlab.com/rhendric/nix-list-index-syntax/-/archive/v2.tar.gz";
    sha256 = "13jyc6f0rmpgs3pryr6mk3ka27gm9ij2f14nb0dlsdn7sxk3vs0m";
  })
];

# Importing the module has no effect on your software by default.
# To use a patch system-wide, uncomment one of the below:
#nix.patches.listIndexSyntax = "bracketed";
#nix.patches.listIndexSyntax = "dot";
#nix.patches.listIndexSyntax = "bang";
```

This has the same effect as adding the overlay described in the next section, but is simpler for users of NixOS.

### As a Nixpkgs overlay

Add the following to your overlays:

```nix
let
  proj = builtins.fetchTarball {
    url = "https://gitlab.com/rhendric/nix-list-index-syntax/-/archive/v2.tar.gz";
    sha256 = "13jyc6f0rmpgs3pryr6mk3ka27gm9ij2f14nb0dlsdn7sxk3vs0m";
  };
in
import "${proj}/overlay.nix" { }
```

The effect of this overlay is to extend derivations of Nix in Nixpkgs to have one more overridable parameter, `listIndexSyntax`.
You can get a build of Nix with one of the patches with the expression `nix.override { listIndexSyntax = "bracketed"; }` (or `"dot"`, or `"bang"`).
These patches have been tested with every tagged Nix release since 2.18 (as of 2025 February 18), so feel free to use this with any of the derivations in `nixVersions` too (except for `nix_2_3`, which is too old; `nix_2_26`, which uses a very complex build structure that [can't currently be overridden](https://github.com/NixOS/nix/issues/12472); and possibly `git` depending on the pinned commit).

The attrset passed as an argument to the overlay may contain a `listIndexSyntax` attribute with value `null` (the default), `"bracketed"`, `"dot"`, or `"bang"`.
As with the NixOS module, this attribute sets the Nixpkgs-wide default patch to use (`null` means do not patch Nix by default).
Overrides, as described above, take precedence over this attribute.

### As a function applied to a Nix derivation

Run `nix-build` on the following:

```nix
let
  pkgs = import <nixpkgs> { };
  proj = builtins.fetchTarball {
    url = "https://gitlab.com/rhendric/nix-list-index-syntax/-/archive/v2.tar.gz";
    sha256 = "13jyc6f0rmpgs3pryr6mk3ka27gm9ij2f14nb0dlsdn7sxk3vs0m";
  };
  listIndexSyntaxPatches = pkgs.callPackage "${proj}/patches.nix" { };
in
# Choose one:
#listIndexSyntaxPatches.bracketed pkgs.nix
#listIndexSyntaxPatches.dot pkgs.nix
#listIndexSyntaxPatches.bang pkgs.nix
```

An advantage of this approach is that it can be used on a custom Nix derivation, not just one that is derived from Nixpkgs.
These patch functions can be used with any derivation of Nix provided that it is version 2.14 or later and doesn't introduce patch conflicts.

Another advantage is that this makes no modifications to anything else in NixOS or Nixpkgs, if you are paranoid about that.
(The previous two approaches make modifications that, by default, only expose new NixOS options or `override` attributes on Nix derivations, which should have no impact if those features aren't used.
But it's up to you to decide how comfortable you are with that assertion.)

## Which patch should I choose?

This set of patches is being released in conjunction with [RFC-0181](https://github.com/NixOS/rfcs/pull/181), a proposal to introduce the bracketed syntax into the Nix language.
So if you are trying these patches in order to determine your level of interest in the RFC, that's the one to try.

## Why bother with `"dot"` at all then?
[why-dot]: #why-bother-with-dot-at-all-then

Because, subjectively, it's really nice.

The one downside of this syntax is that it requires float literals to start with a digit, which is a change that would break backward compatability for Nix.
Otherwise, the parser can't distinguish between `x .0`, an expression that gets the first element out of a list stored in the variable `x`, and `x .0`, an expression that applies a function stored in the variable `x` to the float literal `.0`.

But you shouldn't care about that.
Float literals that start with a bare decimal point do not occur in Nixpkgs.
Probably someone uses them somewhere, but they are a bad idea for both objective and subjective reasons.

The most objective argument goes as follows: the period character is generally the least visually noticable glyph of all the non-whitespace, printable glyphs in a given font.
As a decimal point, it is more noticable for the space it introduces between digits than for the ink or pixels that comprise it.
When a decimal point is not surrounded by digits, it can be missed.
At a glance, it is easy to confuse ‘1001.’ and ‘1001’; this is of course harmless.
To confuse ‘.1001’ and ‘1001’ is a bigger deal.

If the maintainers of Nix were willing to deprecate and remove obsolete syntax, this would be a great place to do so, along with some other [infelicities](https://github.com/NixOS/nix/issues/8605) associated with float literals while they're at it.
[Currently](https://github.com/NixOS/rfcs/pull/137#issuecomment-1746740433), however, they are not.

But that doesn't need to stop you, dear reader, from adopting this patch for your personal use.

## What's the case for `"bang"`?

The ergonomics of `"dot"` without the need for a breaking language change.
The `!` character is like `.` but different.
It's also the operator used to index into arrays and vectors in Haskell, for users with that shared background.

`!` is used as a prefix operator in Nix, but it's an error to apply it to a number.
So there's no conflict between `a!b`, which is the application of `a` to not-`b`, and `a!0`, which is the first element of `a`.
