let
  pkgs = import <nixpkgs> { };
  inherit (pkgs) ccacheStdenv fetchpatch2 gitMinimal lib linkFarm toml11;

  pkgs' = import <nixpkgs> { overlays = [ (import dist/overlay.nix { }) ]; };
  inherit (pkgs') nixVersions;

  patch-rapidcheck-shared = fetchpatch2 {
    # https://github.com/NixOS/nix/pull/9431
    name = "fix-missing-librapidcheck.patch";
    url = "https://github.com/NixOS/nix/commit/46131567da96ffac298b9ec54016b37114b0dfd5.patch";
    hash = "sha256-lShYxYKRDWwBqCysAFmFBudhhAL1eendWcL8sEFLCGg=";
  };

  hashes = {
    "2.25.5" = "sha256-9xrQhrqHCSqWsQveykZvG/ZMu0se66fUQw3xVSg6BpQ=";
    "2.25.4" = "sha256-cB/1vIYk8LWvL71hiKFu8froJHTUAfsYOOxBlBeNglI=";
    "2.25.3" = "sha256-T+wFMm3cj8pGJSwXmPuxG5pz+1gRDJoToF9OBxtzocA=";
    "2.25.2" = "sha256-MZNpb4awWHXU+kGmH58VUB7M9l6UVo33riuQLTbMh4E=";
    "2.25.1" = "sha256-+iEKR6G8siXUY7IaohJiPlIzmo826R9LyLRyx3DD7Jc=";
    "2.25.0" = "sha256-+cJZDy1+5jI+Utf6jM7pW7OvLmPVFQGwbAIjGVZL/48=";
    "2.24.12" = "sha256-lPiheE0D146tstoUInOUf1451stezrd8j6H6w7+RCv8=";
    "2.24.11" = "sha256-ZizmbJM+DbhkaizxbjKg9fNfMrxh3PfAZ6jApQrazks=";
    "2.24.10" = "sha256-XdeVy1/d6DEIYb3nOA6JIYF4fwMKNxtwJMgT3pHi+ko=";
    "2.24.9" = "sha256-OwJByTdCz1t91ysBqynK+ifszkoIGEXUn6HE2t82+c8=";
    "2.24.8" = "sha256-YPJA0stZucs13Y2DQr3JIL6JfakP//LDbYXNhic/rKk=";
    "2.24.7" = "sha256-NAyc5MR/T70umcSeMv7y3AVt00ZkmDXGm7LfYKTONfE=";
    "2.24.6" = "sha256-kgq3B+olx62bzGD5C6ighdAoDweLq+AebxVHcDnKH4w=";
    "2.24.5" = "sha256-mYvdPwl4gcc17UAomkbbOJEgxBQpowmJDrRMWtlYzFY=";
    "2.24.4" = "sha256-oYu/9u8ht34JOTV+G/l3CCFJokPiUA2D8CiLZFX61PA=";
    "2.24.3" = "sha256-aBuGXm0UwDekCYLl7xDyw+BAJOg7728i57TbSXzPacc=";
    "2.24.2" = "sha256-ne4/57E2hOeBIc4yIJkm5JDIPtAaRvkDPkKj7pJ5fhg=";
    "2.24.1" = "sha256-3yFEvUDPB7GlCMI9I5VV+HXMVOT38h3lnw01nIXU2F4=";
    "2.24.0" = "sha256-ZoCDnADa9Pa4KfAknLI7lZF2+N5+CwBqJkA89rbSAWI=";
    "2.23.4" = "sha256-rugH4TUicHEdVfy3UuAobFIutqbuVco8Yg/z81g7clE=";
    "2.23.3" = "sha256-lAoLGVIhRFrfgv7wcyduEkyc83QKrtsfsq4of+WrBeg=";
    "2.23.2" = "sha256-NH1G4GGHXU4pnQWI9X/gs7r97GO0i4tJFjoaIxl0FaQ=";
    "2.23.1" = "sha256-US+UsPhFeYoJH0ncjERRtVD1U20JtVtjsG+xhZqr/nY=";
    "2.23.0" = "sha256-cRCwRDxR8rEQQEvGjIH8g0krJd4ZFJrdgmPXkv65S/Y=";
    "2.22.3" = "sha256-l04csH5rTWsK7eXPWVxJBUVRPMZXllFoSkYFTq/i8WU=";
    "2.22.2" = "sha256-/ABtOUnfN6od/MtLxO5cJX90Ags/vOBQav6MyhKy4r4=";
    "2.22.1" = "sha256-5Q1WkpTWH7fkVfYhHDc5r0A+Vc+K5xB1UhzrLzBCrB8=";
    "2.22.0" = "sha256-Ugcc+lSq8nJP+mddMlGFnoG4Ix1lRFHWOal3299bqR8=";
    "2.21.5" = "sha256-/+TLpd6hvYMJFoeJvVZ+bZzjwY/jP6CxJRGmwKcXbI0=";
    "2.21.4" = "sha256-c6nVZ0pSrfhFX3eVKqayS+ioqyAGp3zG9ZPO5rkXFRQ=";
    "2.21.3" = "sha256-oaRT9rGrYKxebZ6aiBFTx2OwcIDQthkzRdeez3EARH4=";
    "2.21.2" = "sha256-ObaVDDPtnOeIE0t7m4OVk5G+OS6d9qYh+ktK67Fe/zE=";
    "2.21.1" = "sha256-iRtvOcJbohyhav+deEajI/Ln/LU/6WqSfLyXDQaNEro=";
    "2.21.0" = "sha256-9b9qJ+7rGjLKbIswMf0/2pgUWH/xOlYLk7P4WYNcGDs=";
    "2.20.9" = "sha256-b7smrbPLP/wcoBFCJ8j1UDNj0p4jiKT/6mNlDdlrOXA=";
    "2.20.8" = "sha256-M2tkMtjKi8LDdNLsKi3IvD8oY/i3rtarjMpvhybS3WY=";
    "2.20.7" = "sha256-hWElUtAHYbL/LjyW0Vovz9zJLhv5zC6/tDu8uPkbQqY=";
    "2.20.6" = "sha256-BSl8Jijq1A4n1ToQy0t0jDJCXhJK+w1prL8QMHS5t54=";
    "2.20.5" = "sha256-bfFe38BkoQws7om4gBtBWoNTLkt9piMXdLLoHYl+vBQ=";
    "2.20.4" = "sha256-Sv5VFPF5BAXkMWgekh0iH1SeqTF8VcCiW5nR6/AATrI=";
    "2.20.3" = "sha256-s7QTMxLzVA5UF80sFCv8jwaTMBLA8/110YFkZNkNsCk=";
    "2.20.2" = "sha256-avydDtF3k+FfBMh60TIVqoqPy0mjqwjZnw1gFba+7T0=";
    "2.20.1" = "sha256-tLO1Y08d+1K1Tm8UpLdnx7bi3vR5dhfuZho5S/RPQ0s=";
    "2.20.0" = "sha256-z/s0OwYBUqnLiEv3cLJZiaje52snOWOuuoqkGAXKjsI=";
    "2.19.7" = "sha256-CkT1SNwRYYQdN2X4cTt1WX3YZfKZFWf7O1YTEo1APfc=";
    "2.19.6" = "sha256-XT5xiwOLgXf+TdyOjbJVOl992wu9mBO25WXHoyli/Tk=";
    "2.19.5" = "sha256-n4efeDi8KtLgkAKl5kBQ4svmdxfnRp8KrSZGrlFsr/E=";
    "2.19.4" = "sha256-qXjyVmDm1SFWk1az3GWIsJ0fVG0nWet2FdldFOnUydI=";
    "2.19.3" = "sha256-EtL6M0H5+0mFbFh+teVjm+0B+xmHoKwtBvigS5NMWoo=";
    "2.19.2" = "sha256-iA8DqS+W2fWTfR+nNJSvMHqQ+4NpYMRT3b+2zS6JTvE=";
    "2.19.1" = "sha256-OzAeQwlAF4l0h2uBWGIPvGBYNL6MpBfrdRKwHTRQXl4=";
    "2.19.0" = "sha256-7RXZG0HRRjnTDCleaKvboVox4ZlJAW+1l12ulzf3WI4=";
    "2.18.9" = "sha256-RrOFlDGmRXcVRV2p2HqHGqvzGNyWoD0Dado/BNlJ1SI=";
    "2.18.8" = "sha256-0rHRifdjzzxMh/im8pRx6XoY62irDTDUes+Pn0CR65I=";
    "2.18.7" = "sha256-ZfcL4utJHuxCGILb/zIeXVVbHkskgp70+c2IitkFJwA=";
    "2.18.6" = "sha256-OZDAxtIEwZIWv3+zvphZiBU/Ohlv6EjABYUjrTPYYr4=";
    "2.18.5" = "sha256-xEcYQuJz6DjdYfS6GxIYcn8U+3Hgopne3CvqrNoGguQ=";
    "2.18.4" = "sha256-tebjp83ABKrBW3d/6n/Irr1+xhtw8qIkqHZHJOoJaLk=";
    "2.18.3" = "sha256-430V4oN1Pid0h3J1yucrik6lbDh5D+pHI455bzLPEDY=";
    "2.18.2" = "sha256-8gNJlBlv2bnffRg0CejiBXc6U/S6YeCLAdHrYvTPyoY=";
    "2.18.1" = "sha256-WNmifcTsN9aG1ONkv+l2BC4sHZZxtNKy0keqBHXXQ7w=";
    "2.18.0" = "sha256-ggISAuGpTkKp6JXt1BbcLtLDYOPECWqrVnPVgQEFHYc=";
  };

  flavors = name: pkg:
    lib.mapAttrs'
      (syntax: _: { name = "${name}-${syntax}"; value = pkg.override { stdenv = ccacheStdenv; listIndexSyntax = syntax; }; })
      (import dist/patch-data.nix);

  overrideArgs = lib.functionArgs nixVersions.latest.override;

  makeOverride = { version, rev, hash }: oldAttrs:
    {
      inherit version;
      src = oldAttrs.src.override { inherit hash rev; };
      doCheck = true;
      nativeCheckInputs = oldAttrs.nativeCheckInputs or [ ] ++ [ gitMinimal ];
      patches =
        let
          missingRapidCheck = lib.versionOlder version (if lib.versionAtLeast version "2.19" then "2.19.2" else "2.18.2");
        in
        oldAttrs.patches or [ ] ++ lib.optional missingRapidCheck patch-rapidcheck-shared;
    } // lib.optionalAttrs (lib.versionAtLeast version "2.24pre") {
      env.testresults = "$TMPDIR";
    };
in

# Test the new arg...
assert overrideArgs.listIndexSyntax;
# ... and an old arg
assert overrideArgs.withAWS;

linkFarm "all-patched-nixes" (
  lib.concatMapAttrs (version: hash:
    flavors version (
      (nixVersions."nix_2_${lib.versions.minor version}" or nixVersions.latest).overrideAttrs
        (makeOverride { inherit hash version; rev = version; })
    )
  ) hashes
  // flavors "git" nixVersions.git
)
